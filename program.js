const readline = require('readline');
const fs=require('fs');
const os=require('os');
const http=require('http');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question("Hello and welcome to this awesome project \n Please enter one of the following: \n 1, Read package.json \n 2, Display OS info \n 3, Start Http server", (answer) => {
  // TODO: Log the answer in a database
  if(answer==1){
      readPackageJson();
  }
  else if(answer==2){
      displayJson();
  }
  else if(answer==3){
      startHTTPServer();
  }
  else{
      console.log("Illegal input");
  }

  rl.close();
});

function readPackageJson(){
fs.readFile(__dirname+'/package.json','utf-8',(err,content)=>{
    console.log(content);
})
}
function displayJson(){
    console.log('SYSTEM MEMORY',(os.totalmem()/1024/1024/1024).toFixed(2)+ ' GB ');
    console.log("Number of CPU's:"+ os.cpus().length);
    console.log("Free memory: "+os.freemem());
    console.log("Architecture"+os.arch());
    console.log("Platform: "+os.platform());
    console.log("User: "+os.userInfo().username);
}
function startHTTPServer(){
    const server=http.createServer((req,res)=>{
        if(req.url==="/"){
            res.write("Hello world!");
            res.end();
        }
        if(req.url==="/projects"){
            res.write(JSON.stringify(['Javascript', 'Node.JS']));
            res.end();
        }
     
    });
    server.listen(3000);
}